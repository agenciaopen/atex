<?php get_header('call'); ?>

<?php
global $post;
$pageID = get_option('page_on_front');

?>
<?php 
    $banner = get_field( 'imagem_destaque', $pageID ); 
    $title = get_field('chamada', $pageID);
    $subtitle = get_field('chamada_desc', $pageID); 
    $form = get_field('formulario_de_pre_cadastro', $pageID);  
?>
<section class="main_banner one_fourth" id="" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('<?php echo $banner;?>')"> 
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-12 text-center">
                <h1 class="text-white"><?php echo $title; ?></h1>
                <h2 class="text-white"><?php echo $subtitle; ?></h2>
        
                <div class="form-inline">
                    <?php echo $form; ?>
                </div>
            </div>
        </div><!--/.container-->
    </div><!--/.row-->
</section><!--/.main_banner-->
<section class="wrapper" id="author-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
						?>
					</header><!-- .page-header -->

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'loop-templates/content', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div> <!-- .row -->

	</div><!-- #content -->

				</section><!-- #archive-wrapper -->

<?php get_footer(); ?>
