<section class="constructions">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-between">
            <div class="col-md-3">
                <h2>Praticidade para toda obra</h2>
                <p>Encontre a melhor solução para p seu projeto.</p>
            </div>
            <div class="col-md-9">
                <ul class="solution-projeto">
                    <?php if ( have_rows( 'solucao_projeto', 'option' ) ) : ?>
	                    <?php while ( have_rows( 'solucao_projeto', 'option' ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'projeto_thumbnail' ) ) : ?>
                                <li class="card_content">
                                    <p><?php the_sub_field( 'projeto_texto' ); ?></p>
			                        <img src="<?php the_sub_field( 'projeto_thumbnail' ); ?>" />
		                            <?php endif ?>
                                </li>
	                    <?php endwhile; ?>
                        <?php else : ?>
	                    <?php // no rows found ?>
                    <?php endif; ?>   
                </ul>
            </div>
        </div>
    </div>
</section><!-- /.constructions -->