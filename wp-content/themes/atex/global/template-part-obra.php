<section class="obra">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
            <?php if ( get_field( 'barra_horizontal', 'option' ) ) : ?>
	            <img class="barra-horrizontal" src="<?php the_field( 'barra_horizontal', 'option' ); ?>" />
            <?php endif ?>
                <h2>Por que escolher Atex para sua obra?</h2>
            </div>

            <div class="col-md-9">
                <ul class="obra_atex">
                    <?php if ( have_rows( 'privilegios', 'option' ) ) : ?>
	                    <?php while ( have_rows( 'privilegios', 'option' ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'privilegios_icons' ) ) : ?>
                                <li class="obra_content">
			                        <img src="<?php the_sub_field( 'privilegios_icons' ); ?>" />
		                            <?php endif ?>
		                            <p><?php the_sub_field( 'privilegios_texto' ); ?></p>
                                </li>
	                    <?php endwhile; ?>
                        <?php else : ?>
	                    <?php // no rows found ?>
                    <?php endif; ?>   
                </ul>
            </div>
            <button class="btn btn_first col-md-4 mt-4 mx-auto text-center mb-4" id="sobre_atex">Mais sobre a Atex</button>
        </div>
    </div>
</section><!-- /.obra -->