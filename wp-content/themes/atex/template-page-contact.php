<?php
/**
*
* Template Name: Contato
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<section id="content">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-md-5 text-center">
                <?php the_field( 'texto_da_pagina', $pageID ); ?>
            </div>
            <div class="col-md-6 text-center">
                <?php 
                    $phone = get_field( 'numero_whatsapp', $pageID );
                    $phone = preg_replace('/\D+/', '', $phone);
                    $message = rawurldecode(get_field( 'mensagem_ao_abrir_whatsapp', $pageID ));
                ?>
                <a href="https://wa.me/<?php echo $phone; ?>?text=<?php echo $message;?>" rel="external" target="_blank">
                    <img src='<?php the_field( 'icone_whatsapp', $pageID ); ?>' class='img-fluid' alt='<?php echo $phone; ?>?text=<?php echo $message;?>' title='<?php echo $phone; ?>?text=<?php echo $message;?>' loading='lazy'>
                    <p><b><?php the_field( 'texto_apos_whatsapp', $pageID ); ?></b></p>
                </a>
            </div>
        </div>
    </div>
</section><!--/.content-->

<?php get_footer(); ?>