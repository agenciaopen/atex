<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
			<div class="col-xl-4 col-md-6 article-wrapper mb-5 post_item">
					<a href="<?php echo get_permalink() ?>">
						
						<?php $img = wp_get_attachment_url(get_post_thumbnail_id($post->ID), '' ); ?>
						<figure class="tint col-md-12 p-0" style="background-image: url('<?php echo $img; ?>')">
						</figure><!--/.figure-->
					
						<div class="col-md-12 p-0">
							<?php 
								global $postcat;
								$postcat = get_the_category( $post->ID );
								$category = get_the_category();
								$firstCategory = $category[0]->cat_name;
								// Get the ID of a given category
								$category_id = get_cat_ID( $firstCategory );
								// Get the URL of this category
								$category_link = get_category_link( $category_id );
							?>
							<p class="col-md-12 p-0"><a href="<?php echo esc_url( $category_link ); ?>"><?php echo $firstCategory; ?></a><p>
							<h2><?php the_title()?></h2>
							<p class=""><?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);?></p>
						</div><!--/.card-body-->
					</a><!--/.permalink-->
				</div><!--/.<?php sanitize_title(get_the_title()); ?>-->