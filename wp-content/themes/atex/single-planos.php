<?php
/**
*
* single page for cpt planos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part( 'planos/template-part', 'banner' ); ?>

    <?php if ( have_rows( 'conteudo_pagina', $page_ID ) ): ?>
        <?php while ( have_rows( 'conteudo_pagina', $page_ID ) ) : the_row(); ?>
            <?php if ( get_row_layout() == 'beneficios' ) : ?>
                <section class="meet">
                    <div class="container h-100">
                        <div class="row h-100 align-items-start justify-content-between">
                            <div class="col-md-12 text-center">
                                <?php the_sub_field( 'titulo' ); ?>
                            </div>
                            <?php if ( have_rows( 'cadastro_de_beneficios' ) ) : ?>
                                <?php while ( have_rows( 'cadastro_de_beneficios' ) ) : the_row(); ?>
                                    <div class="col-md-3 text-center item">
                                        <div class="col-md-12 p-0 icon">
                                            <img src='<?php the_sub_field( 'icone' ); ?>' class='img-fluid' alt='<?php the_sub_field( 'titulo' ); ?>' title='<?php the_sub_field( 'titulo' ); ?>' loading='lazy'>
                                        </div>
                                        <h3><?php the_sub_field( 'titulo' ); ?></h3>
                                        <p><?php the_sub_field( 'descricao', false, false ); ?> </p>
                                    </div>
                                <?php endwhile; ?>
                            <?php else : ?>
                                <?php // no rows found ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section><!-- /.meet -->
                
            <?php elseif ( get_row_layout() == 'duvidas' ) : ?>
                <section class="faq">
                    <div class="container h-100">
                        <div class="row align-items-start justify-content-center h-100">
                            <div class="col-md-10 col-lg-9 text-center">
                                <h2><?php the_sub_field( 'titulod' ); ?></h2>
                                <p><?php the_sub_field( 'descricaod' ); ?></p>
                                <?php 
                                    $count = 0;
                                    $faqTotal = get_sub_field('cadastro_de_duvidas');
                                    if (is_array($faqTotal)) {
                                        $count = count($faqTotal);
                                    }
                                ?>
                            </div>
                            <?php if ( have_rows( 'cadastro_de_duvidas' ) ) : ?>
                                <div class="col-md-12 text-center row m-0 justify-content-between" id="accordion">
                                    <?php $faq = 1; ?>


                                    <?php while ( have_rows( 'cadastro_de_duvidas' ) ) : the_row(); ?>
                                       
                                            <div class="card col-md-12 px-lg-5">
                                                <div class="card-header <?php if ($faq == 1):?> active <?php endif;?>" id="heading<?php echo $faq;?>">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $faq;?>" aria-expanded="true" aria-controls="collapse<?php echo $faq;?>">
                                                            <?php echo $faq;?>. <?php the_sub_field( 'duvida' ); ?>
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapse<?php echo $faq;?>" class="collapse <?php if ($faq == 1):?>show <?php endif;?>" aria-labelledby="heading<?php echo $faq;?>" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <?php the_sub_field( 'resposta' ); ?>
                                                    </div>
                                                </div>
                                            </div>

                                    <?php $faq++; endwhile; ?>
                                </div>
                            <?php else : ?>
                                <?php // no rows found ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section><!--/.faq-->
            <?php elseif ( get_row_layout() == 'planos_content' ) : ?>
                <section class="content plans">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-8 col-lg-7 text-center">
                                <h2> <?php the_sub_field( 'titulop', $page_ID ); ?></h2>
                                <p> <?php the_sub_field( 'descricaop', $page_ID ); ?></p>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="row m-0 justify-content-between align-items-start <?php if(wp_is_mobile()):?> carousel_featured <?php endif;?> ">
                                    <?php 
                                $planos = get_posts( array(
                                    'posts_per_page' => -1,
                                    'post_type' => 'planos'
                                ) );
                                if ( $planos ) {
                                    $count = 1;
                                    foreach ( $planos as $post ) :
                                        setup_postdata( $post ); ?>
                                    <div class="col-md-6 mb-5 mb-lg-0 col-lg-4 px-md-4 text-center text-md-left item <?php if ($page_ID == $post->ID): ?> order-2 <?php endif; ?>">
                                        <div class="col-md-12 p-0 card px-md-4">
                                            <div class="col-md-12 icon p-0">
                                                <img src='<?php the_field( 'icone_plano', $post->ID ); ?>' class='img-fluid' alt='' title='' loading='lazy'>
                                            </div>
                                            <h3><?php the_field( 'subtitulop', $post->ID); ?></h3>
                                            <?php if ( have_rows( 'beneficios_gerais' ) ) : ?>
                                                <ul class="benefits">
                                                    <?php while ( have_rows( 'beneficios_gerais' ) ) : the_row(); ?>
                                                        <li><span><?php the_sub_field( 'textob' ); ?></span></li> 
                                                    <?php endwhile; ?>
                                                    <?php if (get_field('observacao')): ?>
                                                        <li class="obs"><small><?php the_field( 'observacao' ); ?></small></li>
                                                    <?php endif;?>
                                                </ul>
                                            <?php else : ?>
                                                <?php // no rows found ?>
                                            <?php endif; ?>
                                            <?php if ( have_rows( 'cadastro_valores' ) ) : ?>
                                                <div class="list-inline p-0 m-0 row justify-content-between">
                                                    <?php while ( have_rows( 'cadastro_valores' ) ) : the_row(); ?>
                                                        <div class="col-md-5 p-0">
                                                            <span class="badge">
                                                                <?php the_sub_field( 'tipo_plano' ); ?>
                                                            </span>
                                                            <div class="row m-0 align-items-end justify-content-center">
                                                                <div class="col-2 col-md-4 p-0">
                                                                    <p class="installments"><?php the_sub_field( 'parcelas' ); ?>x</p>
                                                                    <p class="sign">R$</p>
                                                                </div>
                                                                <div class="col-3 col-md-8 p-0">
                                                                    <p class="price"><?php the_sub_field( 'valor_parcelado' ); ?></p>
                                                                </div>
                                                                <div class="col-12 p-0">
                                                                    <p><small>ou R$<?php the_sub_field( 'valor_cheio' ); ?> à vista</small></p>
                                                                </div>
                                                            </div>
                                            </div>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php else : ?>
                                                <?php // no rows found ?>
                                            <?php endif; ?>
                                    
                                        </div>
                                        <?php $botao = get_field( 'botao' ); ?>
                                            <?php if ( $botao ) : ?>
                                                <a class="btn col-md-12 btn_default" href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>"><?php echo esc_html( $botao['title'] ); ?></a>
                                            <?php endif; ?>   
                                    </div>
    <?php
    $count++; endforeach; 
    wp_reset_postdata();
}

?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->
            <?php elseif ( get_row_layout() == 'formulário_content' ) : ?>
                <section class="content">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-10 col-lg-9 text-center">
                                <h2><?php the_sub_field( 'titulof' ); ?></h2>
                                <p><?php the_sub_field( 'descricaof', false ,false ); ?></p>
                            </div>
                            <div class="col-md-10">
                                <?php the_sub_field( 'formulario_de_contato' ); ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php else: ?>
        <?php // no layouts found ?>
    <?php endif; ?>

<?php endwhile; ?>


<?php get_footer(); ?>
