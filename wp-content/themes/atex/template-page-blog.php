<?php
/**
*
* Template Name: Blog
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php
if(wp_is_mobile()):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
                else:
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                endif;
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post" style="background-image: url('<?php echo $featured_img_url;?>');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center">
                                <h1>
                                    <?php echo $title;?>
                                </h1>
                            </div>
                        </div>
                    </div>
                </section><!-- /.main -->
                <?php get_template_part( 'templates/global/template-part', 'list-posts' ); ?>


<?php get_footer(); ?>