var top1 = $('.carousel').offset().top;
var top2 = $('.plans').offset().top;
var top3 = $('.main').offset().top;
var top4 = $('.redes').offset().top;
var top5 = $('.testimonials').offset().top;
var top6 = $('.article').offset().top;
var top7 = $('.newsletter').offset().top;

$(document).scroll(function() {
  var scrollPos = $(document).scrollTop();
  if (scrollPos >= top1 && scrollPos < top2) {
    $('#nav_main').removeClass('v2');
    $('#nav_main').addClass('v1');
  } else if (scrollPos >= top2 && scrollPos < top3) {
    $('#nav_main').removeClass('v1');
    $('#nav_main').addClass('v2');
} else if (scrollPos >= top3 && scrollPos < top4) {
    $('#nav_main').removeClass('v1');
    $('#nav_main').addClass('v2');
} else if (scrollPos >= top5 && scrollPos < top6) {
    $('#nav_main').removeClass('v1');
    $('#nav_main').addClass('v2');
} else if (scrollPos >= top6 && scrollPos < top7) {
    $('#nav_main').removeClass('v1');
    $('#nav_main').addClass('v2')
  } else if (scrollPos >= top7) {
    $('#nav_main').removeClass('v2');
    $('#nav_main').addClass('v1');
  }
});