<?php get_template_part( 'global/template-part', 'newsletter' ); ?>
<!-- Footer -->
<footer>
    <div class="container h-100">
        <div class="row h-100 align-items-stretch justify-content-between">
            <div class="col-md-2">
                <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
            </div>
            <div class="col-md-8">
                <ul>
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">Quem somos</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2">
                <div class="col-md-12 p-0 border">
                    <p>Redes Sociais</p>
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12 p-0">
                    <p class="d-none d-md-block">Whatsapp</p>
                    <?php 
                    $phone = get_field( 'numero_whatsapp', $pageID );
                    $phone = preg_replace('/\D+/', '', $phone);
                    $message = rawurldecode(get_field( 'mensagem_ao_abrir_whatsapp', $pageID ));
                    ?>
                    <a href="https://wa.me/<?php echo $phone; ?>?text=<?php echo $message;?>" rel="external" target="_blank">
                        <i class="fas fa-whatsapp"></i><?php echo $phone; ?>
                    </a>
                </div>
                <div class="col-md-12 p-0">
                    <p class="d-none d-md-block">Telefone</p>
                    <?php 
                    $phone_tel = get_field( 'numero_telefone', $pageID );
                    $phone_tel = preg_replace('/\D+/', '', $phone_tel);
                    ?>
                    <a href="tel:+<?php echo $phone_tel; ?>" rel="external" target="_blank">
                        <i class="fas fa-phone"></i><?php echo $phone_tel; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

  
<?php wp_footer(); ?>

</body>

</html>
